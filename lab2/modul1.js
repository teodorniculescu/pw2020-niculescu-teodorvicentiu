const sumaVector = (vec) => { 
	var rezultat = 0;
	const reducer = (accumulator, currentValue) => accumulator + currentValue;
	rezultat = vec.reduce(reducer);
	return rezultat;
}
module.exports = {
	sumaVector
};

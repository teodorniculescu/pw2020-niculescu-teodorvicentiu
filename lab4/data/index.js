const { Pool } = require('pg');

const pool = new Pool({
  user: process.env.PGUSER,//'root',
  host: process.env.PGHOST, //'localhost',
  database: process.env.PGDATABASE, //'database',
  password: process.env.PGPASSWORD,//'root',
  port: process.env.PGPORT,//5432
});

const query = async (text, params) => {
  const start = Date.now();
  const {
    rows,
  } = await pool.query(text, params);
  const duration = Date.now() - start;
  console.log(`Query took ${duration} and returned ${rows.length} rows.`);
  return rows;
};

module.exports = {
  query,
};

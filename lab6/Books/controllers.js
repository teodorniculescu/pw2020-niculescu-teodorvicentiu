const express = require('express');

const BooksService = require('./services.js');
const {
    validateFields
} = require('../utils');
const {
    authorizeAndExtractToken
} = require('../security/Jwt');
const {
    ServerError
} = require('../errors');
const {
    authorizeRoles
} = require('../security/Roles');

const router = express.Router();

const possibleGenres =  ['horror', 'fiction', 'romance', 'science-fiction', 'fantasy', 'philosophy', 'biography'];

router.post('/', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        name,
        authorId,
        genres
    } = req.body;
    try {
        const fieldsToBeValidated = {
            name : {
                value: name ,
                type: 'ascii'
            },
            authorId : {
                value: authorId,
                type: 'ascii'
            },
		genres: {
			value: genres,
			type: 'ascii'
		}
		
        };
	    validateFields(fieldsToBeValidated);
	    if (possibleGenres.indexOf(genres) < 0)
		    throw new ServerError(`Campul genres trebuie sa contina doar un entry valid din lista: ${possibleGenres}`, 400);
	    await BooksService.add(name, authorId, genres);
		res.status(201).end();
        // do logic
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        // pot sa primesc eroare si ca genul nu e bun, trebuie verificat mesajul erorii
        // HINT err.message
        next(err);
    }
});

router.get('/', authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    try {
        const books = await BooksService.getAll();
        res.json(books);
        // do logic
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.get('/:id', authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        // do logic
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        const book = await BooksService.getById(id);
        res.json(book);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.get('/authors/:id', authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        const book = await BooksService.getByAuthorId(id);
        res.json(book);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.put('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
        name,
        authorId,
        genres
    } = req.body;
    try {
        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'ascii'
            },
            name : {
                value: name ,
                type: 'ascii'
            },
            authorId : {
                value: authorId,
                type: 'ascii'
            },
		genres: {
			value: genres,
			type: 'ascii'
		}
	};
        validateFields(fieldsToBeValidated);
	    if (possibleGenres.indexOf(genres) < 0)
		    throw new ServerError(`Campul genres trebuie sa contina doar un entry valid din lista: ${possibleGenres}`, 400);

        await BooksService.updateById(id, name, authorId, genres);
        res.status(204).end();
		
       // do logic
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 

        // pot sa primesc eroare si ca genul nu e bun, trebuie verificat mesajul erorii
        // HINT err.message 
        next(err);
    }
});

router.delete('/:id', authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {
        validateFields({
            id: {
                value: id,
                type: 'ascii'
            }
        });
        // se poate modifica 
        await BooksService.deleteById(id);
        res.status(204).end();
        // do logic
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

module.exports = router;

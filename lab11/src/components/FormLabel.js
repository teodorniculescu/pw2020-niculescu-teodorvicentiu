import React from "react";

class FormLabel extends React.Component {
  render() {
    return (
      <label>
        {this.props.name.toUpperCase()}
        <input
          type="text"
          name={this.props.name}
          onChange={this.props.change}
        />
      </label>
    );
  }
}
export default FormLabel;

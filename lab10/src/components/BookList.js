import React from "react";
import axios from "axios";

class BookList extends React.Component {
  state = {
    data: [],
  };
  getBookLists = (event) => {
    const address = "http://localhost:3000/api/v1/books";
    const token = localStorage.getItem("token");
    axios
      .get(address, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        this.setState({ data: res.data });
        console.log(this.state.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  formatData = () => {
    return (
      <ul>
        {this.state.data.map((entry) => (
          <li key={entry._id}>{entry.name + " - " + entry.genres}</li>
        ))}
      </ul>
    );
  };
  render() {
    return (
      <div>
        <button type="button" onClick={this.getBookLists}>
          Click Me!
        </button>
        {this.formatData()}
      </div>
    );
  }
}

export default BookList;

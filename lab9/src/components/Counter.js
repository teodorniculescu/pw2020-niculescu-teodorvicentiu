import React, { useEffect, useState } from "react";
import styles from "./Counter.module.scss";

function formatCounter(counter) {
  return counter === 0 ? "None" : counter;
}

function Counter(props) {
  const [counter, setCounter] = useState(props.value);

  useEffect(() => {
    if (counter === 0) {
      alert("Alerta! Counter-ul este 0!");
    }
  });

  return (
    <div>
      <span className={styles.text}>
        {formatCounter(counter)}
        <br />
      </span>

      <button className={styles.button} onClick={() => props.rst(setCounter)}>
        reset
      </button>
      <button
        className={styles.button}
        onClick={() => props.dec(counter, setCounter)}
      >
        decrement
      </button>
      <button
        className={styles.button}
        onClick={() => props.inc(counter, setCounter)}
      >
        increment
      </button>
    </div>
  );
}

export default Counter;

import React from "react";
import axios from "axios";
import "./Authenticate.module.scss";

class Authenticate extends React.Component {
  state = {
    username: "",
    password: "",
    output: "",
  };
  handleUsernameChange = (event) => {
    this.setState({ username: event.target.value });
  };
  handlePasswordChange = (event) => {
    this.setState({ password: event.target.value });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    const payload = {
      password: this.state.password,
      username: this.state.username,
    };
    const address = "http://localhost:3001/api/v1/users/login";

    axios
      .post(address, payload)
      .then((res) => {
        this.setState({ output: "User sucessfully authenticated!" });
        localStorage.setItem("token", res.data);
      })
      .catch((error) => {
        this.setState({ output: "error" });
        console.log(error);
      });
  };
  handleRegister = (event) => {
    event.preventDefault();
    const payload = {
      password: this.state.password,
      username: this.state.username,
    };
    const address = "http://localhost:3001/api/v1/users/register";

    axios
      .post(address, payload)
      .then((res) => {
        this.setState({ output: "User sucessfully registered!" });
      })
      .catch((error) => {
        this.setState({ output: "error" });
        console.log(error);
      });
  };
  formatOutput = () => {
    return this.state.output;
  };

  render() {
    return (
      <div>
        <form>
          <label>
            Username:
            <input
              type="text"
              name="username"
              onChange={this.handleUsernameChange}
            />
          </label>
          <label>
            Password:
            <input
              type="text"
              name="password"
              onChange={this.handlePasswordChange}
            />
          </label>
          <button onClick={this.handleSubmit}>Submit</button>
          <button onClick={this.handleRegister}>Register</button>
        </form>
        {this.formatOutput()}
      </div>
    );
  }
}

export default Authenticate;

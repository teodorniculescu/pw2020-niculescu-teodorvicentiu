const Router = require('express')();

const AuthorsController = require('../Authors/controllers.js');
const UsersController = require('../Users/controllers.js');

Router.use('/authors', AuthorsController);
Router.use('/users', UsersController);

const BooksController = require('../Books/controllers.js');
Router.use('/books', BooksController);

const PublishersController = require('../Publishers/controllers.js');
Router.use('/publishers', PublishersController);


module.exports = Router;

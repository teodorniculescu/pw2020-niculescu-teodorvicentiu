import React from "react";
import axios from "axios";

class AuthorsList extends React.Component {
  state = {
    data: [],
  };
  getAuthorsLists = (event) => {
    const address = "http://localhost:3000/api/v1/authors";
    const token = localStorage.getItem("token");
    console.log(token);
    axios
      .get(address, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        this.setState({ data: res.data });
        console.log(this.state.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  formatData = () => {
    return (
      <ul>
        {this.state.data.map((entry) => (
          <li key={entry._id}>{entry.firstName + " " + entry.lastName}</li>
        ))}
      </ul>
    );
  };
  render() {
    return (
      <div>
        <button type="button" onClick={this.getAuthorsLists}>
          Click Me!
        </button>
        {this.formatData()}
      </div>
    );
  }
}

export default AuthorsList;

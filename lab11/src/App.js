import React from "react";
import "./App.module.scss";
import Nav from "./components/Nav";
import Book from "./components/Book";
import Author from "./components/Author";
import BookList from "./components/BookList";
import AuthorsList from "./components/AuthorsList";
import Authenticate from "./components/Authenticate.js";
import { HashRouter, Switch, Route } from "react-router-dom";

const path = "http://localhost:3001/api/v1/";
function App() {
  return (
    <HashRouter basename="/">
      <div className="App">
        <Nav />
        <Switch>
          <Route exact path={"/"} component={Home} />
          <Route exact path={"/authenticate"} component={Authenticate} />
          <Route exact path={"/book"} component={Book} />
          <Route exact path={"/author"} component={Author} />
          <Route exact path={"/booklist"} component={BookList} />
          <Route exact path={"/authorlist"} component={AuthorsList} />
        </Switch>
      </div>
    </HashRouter>
  );
}

const Home = () => (
  <div>
    <h1>Home</h1>
  </div>
);

export { App, path };

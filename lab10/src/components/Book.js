import React from "react";
import axios from "axios";

class Book extends React.Component {
  state = {
    data: [],
  };
  formatData = () => {
    return (
      <div>
        {"Name: " + this.state.data.name + " Genres: " + this.state.data.genres}
      </div>
    );
  };
  componentDidMount() {
    const id = this.props.match.params.id;
    const address = `http://localhost:3000/api/v1/books/${id}`;
    const token = localStorage.getItem("token");
    axios
      .get(address, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        this.setState({ data: res.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    return this.formatData();
  }
}

export default Book;

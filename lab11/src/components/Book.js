import React from "react";
import axios from "axios";
import { path } from "../App";
import FormLabel from "./FormLabel";

class Book extends React.Component {
  state = {
    name: "",
    authorid: "",
    genres: "",
  };
  nameChange = (event) => {
    this.setState({ name: event.target.value });
  };
  authoridChange = (event) => {
    this.setState({ authorid: event.target.value });
  };
  genresChange = (event) => {
    this.setState({ genres: event.target.value });
  };
  submit = (event) => {
    event.preventDefault();
    const address = path + "books";
    const payload = {
      name: this.state.name,
      authorId: this.state.authorid,
      genres: this.state.genres,
    };
    const token = localStorage.getItem("token");
    const authorization = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const successMessage = "Added Book!";

    axios
      .post(address, payload, authorization)
      .then((res) => {
        this.setState({ output: successMessage });
        this.props.onResponse();
      })
      .catch((error) => {
        this.setState({ output: "error" });
        console.log(error);
      });
  };
  formatOutput = () => {
    return this.state.output;
  };
  render() {
    return (
      <div>
        <form>
          <fieldset>
            <legend>Add New Book:</legend>
            <FormLabel name="name" change={this.nameChange} />
            <FormLabel name="authorid" change={this.authoridChange} />
            <FormLabel name="genres" change={this.genresChange} />
            <button onClick={this.submit}>Submit</button>
          </fieldset>
        </form>
        {this.formatOutput()}
      </div>
    );
  }
}

export default Book;

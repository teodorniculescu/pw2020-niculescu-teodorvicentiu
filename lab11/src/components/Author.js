import React from "react";
import axios from "axios";
import { path } from "../App";
import FormLabel from "./FormLabel";

class Author extends React.Component {
  state = {
    first: "",
    last: "",
  };
  firstChange = (event) => {
    this.setState({ first: event.target.value });
  };
  lastChange = (event) => {
    this.setState({ last: event.target.value });
  };
  submit = (event) => {
    event.preventDefault();
    const address = path + "authors";
    const payload = {
      firstName: this.state.first,
      lastName: this.state.last,
    };
    const token = localStorage.getItem("token");
    const authorization = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    const successMessage = "Added Author!";

    axios
      .post(address, payload, authorization)
      .then((res) => {
        this.setState({ output: successMessage });
        this.props.onResponse();
      })
      .catch((error) => {
        this.setState({ output: "error" });
        console.log(error);
      });
  };
  formatOutput = () => {
    return this.state.output;
  };
  render() {
    return (
      <div>
        <form>
          <fieldset>
            <legend>Add New Author:</legend>
            <FormLabel name="firstname" change={this.firstChange} />
            <FormLabel name="lastname" change={this.lastChange} />
            <button onClick={this.submit}>Submit</button>
          </fieldset>
        </form>
        {this.formatOutput()}
      </div>
    );
  }
}

export default Author;

const express = require('express');

//Rutele trebuie definite intr-un ruter extern
const route_insert_book = require('./book_route.js');
const db = require('./database.js');

const app = express();
//Rutele de inserare si actualizare vor opera pe JSON, deci trebuie sa activati parasarea de JSON
app.use(express.json());

app.use('/book', route_insert_book);

app.listen(3000, () => {console.log('App listening on port 3000');})

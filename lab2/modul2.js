const myModule1 = require('./modul1.js');

const functie2 = (vec, parNum) => {
	var newvec = vec.filter(elem => elem % parNum == 0);
	var rezultat = myModule1.sumaVector(newvec);
	return rezultat;
}

module.exports = {
	functie2	
};

import React from "react";
import "./Nav.module.scss";
import { Link } from "react-router-dom";

function Nav() {
  return (
    <nav>
      <h3>Navigation</h3>
      <ul>
        <Link to="/">
          <li>Home</li>
        </Link>
        <Link to="/authenticate">
          <li>Authenticate</li>
        </Link>
        <Link to="/book">
          <li>Book</li>
        </Link>
        <Link to="/author">
          <li>Author</li>
        </Link>
        <Link to="/booklist">
          <li>BookList</li>
        </Link>
        <Link to="/authorlist">
          <li>AuthorList</li>
        </Link>
      </ul>
    </nav>
  );
}

export default Nav;

const express = require('express');
const router = express.Router();

const db = require('./database.js');

//O ruta pentru actualizarea unei carti dupa id
router.put('/:id', (req, res) => {
	try {
		const paramId = req.params.id;
		const body = req.body;
		const payload = db.getFromDbById(paramId);
		if (typeof body.autor !== 'undefined')
		{
			payload.autor = body.autor;
		}
		if (typeof body.id !== 'undefined')
		{
			payload.id = body.id;
		}
		if (typeof body.nume !== 'undefined')
		{
			payload.nume = body.nume;
		}
		db.updateById(paramId, payload);
		console.log(db.getAllFromDb());
		res.end();
	} catch (e) {
		res.status(404).send('ID Not Found!');
	}

})

//O ruta pentru stergerea unei carti dupa id dat ca parametru de cale 
router.delete('/:id', (req, res) => {
	const paramId = req.params.id;
	db.removeFromDbById(paramId);
	res.end();
});

// O ruta pentru stergerea intregii baze de date
router.delete('/', (req, res) => {
	db.purgeDb();
	res.end();
});

//O ruta pentru stergerea mai multor carti dupa autor dat ca parametru de cerere 
router.delete('/:id', (req, res) => {
	const queryAuthor = req.query.autor;
	db.removeFromDbByAuthor(queryAuthor);
	res.end();
});

router.get('/', (req, res) => {
	const queryAuthor = req.query.autor;
	// O ruta pentru afisarea unei carti dupa autor dat ca parametru de cerere 
	if (typeof queryAuthor !== 'undefined')
	{
		try {
			console.log(queryAuthor);
			const result = db.getFromDbByAuthor(queryAuthor);
			//console.log(result);
			res.json(result);
		} catch (e) {
			res.status(404).send('Author Not Found!');
		}
	}
	// O ruta pentru afisarea tuturor cartilor
	else
	{
		const result = db.getAllFromDb();
		res.json(result);
	}
})

// O ruta pentru afisarea unei carti dupa id dat ca parametru de cale 
router.get('/:id', (req, res) => {
	try {
		const paramId = req.params.id;
		const result = db.getFromDbById(paramId);
		res.json(result);
	} catch (e) {
		res.status(404).send('ID Not Found!');
	}
})

// O ruta pentru inserarea unei carti
router.post('/insert', (req, res) => {
	db.insertIntoDb(req.body);
	console.log(req.body);
	res.send(db.getAllFromDb());
})

module.exports = router;

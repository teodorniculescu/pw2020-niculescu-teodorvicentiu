import React from "react";
import axios from "axios";
import styles from "./AuthorsList.module.scss";
import Author from "./Author";
import trash from "./trash-alt-solid.svg";

class AuthorsList extends React.Component {
  state = {
    data: [],
  };
  getAuthorsLists = (event) => {
    const address = "http://localhost:3000/api/v1/authors";
    const token = localStorage.getItem("token");
    console.log(token);
    axios
      .get(address, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        this.setState({ data: res.data });
        console.log(this.state.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  deleteAuthor(id) {
    const address = `http://localhost:3000/api/v1/authors/${id}`;
    const token = localStorage.getItem("token");
    console.log(token);
    axios
      .delete(address, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        this.getAuthorsLists();
      })
      .catch((error) => {
        console.log(error);
      });
  }
  getIcon(id) {
    return (
      <i>
        <img
          src={trash}
          alt=""
          onClick={() => {
            this.deleteAuthor(id);
          }}
        />
      </i>
    );
  }
  getTableRow() {
    return this.state.data.map((student, index) => {
      const { _id, firstName, lastName } = student;
      return (
        <tr key={_id}>
          <th>{this.getIcon(_id)}</th>
          <td>{_id}</td>
          <td>{firstName}</td>
          <td>{lastName}</td>
        </tr>
      );
    });
  }
  formatData = () => {
    return (
      <div className={styles.myTable}>
        <table>
          <tbody>
            <tr>
              <th></th>
              <th>ID</th>
              <th>Firstname</th>
              <th>Lastname</th>
            </tr>
            {this.getTableRow()}
          </tbody>
        </table>
      </div>
    );
  };
  componentDidMount() {
    this.getAuthorsLists();
  }
  render() {
    return (
      <div>
        {this.formatData()}
        <Author
          onResponse={() => {
            this.getAuthorsLists();
          }}
        />
      </div>
    );
  }
}

export default AuthorsList;

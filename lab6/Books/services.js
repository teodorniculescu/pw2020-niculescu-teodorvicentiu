const {
    Books,
	Authors
} = require('../data');

const add = async (name, authorId, genres) => {
    // create new Book obj
    const book = new Books({
	author : authorId,
	name,
	genres
    });
    // save it
	await book.save();
};

const getAll = async () => {
    // get all books
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    return await Books.find({},{createdAt:0, updatedAt:0, __v:0}).populate('author', {_id:0, createdAt:0, updatedAt:0, __v:0});
};

const getById = async (id) => {
    // get book by id
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    return await Books.findById(id, {createdAt:0, updatedAt:0, __v:0}).populate('author', {_id:0, createdAt:0, updatedAt:0, __v:0});
};

const getByAuthorId = async (id) => {
    // get book by author id
    // modify output so author is made of 'author.firstName author.lastName'
	console.log(id);
	return await Books.find({author:id},{createdAt:0, updatedAt:0, __v:0}).populate('author', {_id:0, createdAt:0, updatedAt:0, __v:0});
};

const updateById = async (id, name, authorId, genres) => {
    await Books.findByIdAndUpdate(id, {author:authorId, name, genres});
    // update by id
};

const deleteById = async (id) => {
    // delete by id
    await Books.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    getByAuthorId,
    updateById,
    deleteById
}

import React from "react";
import styles from "./Footer.module.scss";

class Student extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nume: this.props.nume,
      prenume: this.props.prenume,
      facultate: this.props.facultate,
      anul_absolvirii: this.props.anabs,
    };
  }
  render() {
    return (
      <div>
        Nume: {this.state.nume}
        <br />
        Prenume: {this.state.prenume}
        <br />
        Facultate: {this.state.facultate}
        <br />
        Anul absolvirii: {this.state.anul_absolvirii}
        <br />
      </div>
    );
  }
}

class Footer extends React.Component {
  render() {
    return (
      <div className={styles.general}>
        <Student
          nume={"Andrei"}
          prenume={"Popescu"}
          facultate={"Automatica"}
          anabs={2020}
        />
        <Student
          nume={"Gigel"}
          prenume={"Mitica"}
          facultate={"MITj"}
          anabs={2222}
        />
      </div>
    );
  }
}

export default Footer;

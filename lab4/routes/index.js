const Router = require('express')();

const AuthorsController = require('../Authors/controllers.js');
Router.use('/authors', AuthorsController);

const BooksController = require('../Books/controllers.js');
Router.use('/books', BooksController);

const PublishersController = require('../Publishers/controllers.js');
Router.use('/publishers', PublishersController);

module.exports = Router;

import React from "react";
import "./App.module.scss";
import Layout from "./components/Layout";
import Header from "./components/Header";
import Nav from "./components/Nav";
import Footer from "./components/Footer";
import Counter from "./components/Counter";

var increment15 = (counter, setCounter) => {
  setCounter(counter + 15);
};
var decrement15 = (counter, setCounter) => {
  setCounter(counter - 15);
};
var increment32 = (counter, setCounter) => {
  setCounter(counter + 32);
};
var decrement32 = (counter, setCounter) => {
  setCounter(counter - 32);
};
var reset = (setCounter) => {
  setCounter(0);
};

function App() {
  return (
    <Layout>
      <Counter value={3} inc={increment32} dec={decrement32} rst={reset} />
      <Counter value={100} inc={increment15} dec={decrement15} rst={reset} />
      <Header />
      <Nav />
      <Footer />
    </Layout>
  );
}

export default App;

import React from "react";
import styles from "./Nav.module.scss";
import logo from "./cloud-computing.svg";

class NavLeft extends React.Component {
  render() {
    return (
      <div>
        <img src={logo} alt="Logo" class={styles.image}></img>
        <div class={styles.vertical_menu}>
          <a href="#">Home</a>
          <a href="#">Books</a>
          <a href="#">Authors</a>
        </div>
      </div>
    );
  }
}
class NavRight extends React.Component {
  render() {
    return (
      <div class={styles.dropdown}>
        <button class={styles.dropbtn}>Dropdown</button>
        <div class={styles.dropdown_content}>
          <a href="#">LOGOUT</a>
        </div>
      </div>
    );
  }
}

class Nav extends React.Component {
  render() {
    return (
      <div>
        <table>
          <tr>
            <th>
              <NavLeft />
            </th>
            <th>
              <NavRight />
            </th>
          </tr>
        </table>
      </div>
    );
  }
}

export default Nav;

import React from "react";
import axios from "axios";

class Author extends React.Component {
  state = {
    data: [],
  };
  formatData = () => {
    return (
      <ul>
        {this.state.data.map((entry) => (
          <li key={entry._id}>
            {entry.author.firstName +
              " " +
              entry.author.lastName +
              " " +
              entry.genres +
              " " +
              entry.name}
          </li>
        ))}
      </ul>
    );
  };
  componentDidMount() {
    const id = this.props.match.params.id;
    const address = `http://localhost:3000/api/v1/books/authors/${id}`;
    const token = localStorage.getItem("token");
    axios
      .get(address, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        this.setState({ data: res.data });
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    return this.formatData();
  }
}

export default Author;

const {
    query
} = require('../data');

const {
    generateToken,
} = require('../security/Jwt');

const {
    ServerError
} = require('../errors');

const {
    hash,
    compare
} = require('../security/Password');

const bcrypt = require('bcryptjs');

const addRole = async (value) => {
    await query('INSERT INTO roles (value) VALUES ($1)', [value]);
};

const getUsers = async() => {
	return await query('SELECT * FROM users');
}

const getRoles = async() => {
    return await query('SELECT * FROM roles');
};

const add = async (username, password, role_id) => {
    // pas 1: cripteaza parola
	const hashed_pass = hash(password);
    // pas 2: adauga (username, parola cripttata, role_id) in baza de date
	console.log(username);
	console.log(hashed_pass);
	console.log(role_id);
    	return await query('INSERT INTO users (username, password, role_id) VALUES ($1, $2, $3);',
		[username, hash, role_id]);

};

const authenticate = async (username, candidatePassword) => {
    const result = await query(`SELECT u.id, u.password, r.value as role FROM users u 
                                JOIN roles r ON r.id = u.role_id
                                WHERE u.username = $1`, [username]);
    if (result.length === 0) {
        throw new ServerError(`Utilizatorul cu username ${username} nu exista in sistem!`, 400);
    }
    const user = result[0];
	const hash = user.password;
    // pas 1: verifica daca parola este buna (hint: functia compare)
	isMatch = compare(candidatePassword, hash);
    // pas 1.1.: compare returneaza true sau false. Daca parola nu e buna, arunca eroare
		if (!isMatch) {
			console.log("passwords didn't match");
			console.log(candidatePassword);
			console.log(hash);
        		throw new ServerError(`incorrect password`, 400);
		} else {
			console.log("Parola corecta!");
			    // pas 2: genereaza token cu payload-ul: {userId si userRole}
				var payload = {
					userId: user.id,
					userRole: user.role
				}
				token = generateToken(payload);
			    // pas 3: returneaza token
				return token;
		}

};

module.exports = {
    add,
    addRole,
    getRoles,
    authenticate,
	getUsers
}

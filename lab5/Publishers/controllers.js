const express = require('express');

const PublishersService = require('./services.js');

const {
	authorizeRoles
} = require('../security/Roles');

const {
	authorizeAndExtractToken
} = require('../security/Jwt');

const {
    validateFields
} = require('../utils');

const {
    ServerError
} = require('../errors');

const router = express.Router();
// nu a fost inclus in schelet, e necesar pentru extragerea corpurilor JSON din cereri HTTP
router.use(express.json());

router.post('/', authorizeAndExtractToken, authorizeRoles('Admin'), async (req, res, next) => {
    const {
        name
    } = req.body;

    // validare de campuri
    try {

        const fieldsToBeValidated = {
            name: {
                value: name,
                type: 'ascii'
            }
        };

        validateFields(fieldsToBeValidated);

        await PublishersService.add(name);

        res.status(201).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.get('/', authorizeAndExtractToken, authorizeRoles('Admin', 'Utilizator'), async (req, res, next) => {
    try {

        const publishers = await PublishersService.getAll();
        res.json(publishers);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.get('/:id', authorizeAndExtractToken, authorizeRoles('Admin', 'Utilizator'), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {

        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });
        const author = await PublishersService.getById(parseInt(id));
        res.json(author);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.put('/:id', authorizeAndExtractToken, authorizeRoles('Admin'), async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
       	name 
    } = req.body;
    try {

        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'int'
            },
            last_name: {
                value: name,
                type: 'ascii'
            }
        };

        validateFields(fieldsToBeValidated);

        await PublishersService.updateById(parseInt(id), name);
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.delete('/:id', authorizeAndExtractToken, authorizeRoles('Admin'), async (req, res, next) => {
    const {
        id
    } = req.params;

    try {

        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });
        // se poate modifica 
        await PublishersService.deleteById(parseInt(id));
        res.status(204).end();
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

module.exports = router;

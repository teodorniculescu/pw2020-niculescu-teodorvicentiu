import React from 'react';
import './App.css';
import Counter from './components/Counter';
import Header from './components/Header';
import Nav from './components/Nav';
import Footer from './components/Footer';
import Layout from './components/Layout';

class App extends React.Component {
 render() {
  return (
   <div>
    <Layout>
     <Header/>
     <Nav/>
     <Counter/>
     <Footer/>
    </Layout>
   </div>
  );
 }
}

export default App;

import React from 'react';

class Counter extends React.Component {
 state = {
  counter: 0
 };

 incrementCounter () {
  this.setState({ counter: this.state.counter + 1 });
 }

 decrementCounter () {
  this.setState({ counter: this.state.counter - 1 });
 }

 resetCounter () {
  this.setState({ counter: 0 });
 }

 formatCounter () {
  return this.state.counter === 0 ? "Nothing" : this.state.counter;
 }

 buttonStyle = {
  padding: "5px",
  color: "white",
  backgroundColor: "Red",
  fontSize: 25
 };

 textStyle = {
  color: "white",
  backgroundColor: "DodgerBlue",
  padding: "10px",
  fontSize: 20
 };

 render() {
  return (
   <div>
    <span style = {this.textStyle}>
     {this.formatCounter()}
     <br/>
    </span>
    <button style= {this.buttonStyle} onClick={() => this.incrementCounter()}>
     Increment
    </button>
    <button style= {this.buttonStyle} onClick={() => this.decrementCounter()}>
     Decrement
    </button>
    <button style= {this.buttonStyle} onClick={() => this.resetCounter()}>
     Reset
    </button>
   </div>
  );
 }
}

export default Counter;

import React from "react";
import axios from "axios";
import styles from "./BookList.module.scss";
import Book from "./Book";

class BookList extends React.Component {
  state = {
    data: [],
  };
  getAuthorsLists = (event) => {
    const address = "http://localhost:3000/api/v1/books";
    const token = localStorage.getItem("token");
    console.log(token);
    axios
      .get(address, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        this.setState({ data: res.data });
        console.log(this.state.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  getTableRow() {
    return this.state.data.map((student, index) => {
      const genres = student.genres;
      const _id = student._id;
      const name = student.name;
      return (
        <tr key={_id}>
          <td>{_id}</td>
          <td>{name}</td>
          <td>{genres}</td>
        </tr>
      );
    });
  }
  formatData = () => {
    return (
      <div className={styles.myTable}>
        <table>
          <tbody>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Genres</th>
            </tr>
            {this.getTableRow()}
          </tbody>
        </table>
      </div>
    );
  };
  componentDidMount() {
    this.getAuthorsLists();
  }
  render() {
    return (
      <div>
        {this.formatData()}
        <Book
          onResponse={() => {
            this.getAuthorsLists();
          }}
        />
      </div>
    );
  }
}

export default BookList;
